terraform {
  backend "s3" {
    bucket = "terraform-iam-authoriser-pipeline-terraform-state"
    key    = "terraform.tfstate"
    region = "eu-west-2"
  }
}