Using Iam authoriser with a Gitlab Pipeline to analyse IAM resources created by Terraform.

- How can we view results ?
- What to use as a trigger to run the job ?
- What if no IAM resources ? 
- What to do with results (exit code) ?



Set Gitlab CI Env Vars 

```
Environment variable name	Value
AWS_ACCESS_KEY_ID	Your Access key ID
AWS_SECRET_ACCESS_KEY	Your Secret access key
AWS_DEFAULT_REGION	Your region code
```

Create a bucket

```bash 
aws s3 mb terraform-iam-authoriser-pipeline-terraform-state
```

Add backend.tf

```
terraform {
  backend "s3" {
    bucket = "terraform-iam-authoriser-pipeline-terraform-state"
    key    = "terraform.tfstate"
    region = "eu-west-2"
  }
}
```

Terraform init 